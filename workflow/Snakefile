'''
This workflow can download and fix assembly sequences and assembly annotations
for all assemblies of interest. It can also download RNAseq data and then clean,
map, estimate coverage/TPM and analyse differential expression in these datasets.
'''

# Path of the config file
configfile: 'config/config.yaml'


# Rule to implement general utility functions
include: 'rules/commons.smk'


# Rules to execute workflow
include: 'rules/files_DL.smk'
include: 'rules/read_preprocessing.smk'
include: 'rules/read_mapping.smk'
include: 'rules/QC.smk'
include: 'rules/tracks.smk'


# Master rule that launches the workflow
rule all:
    '''
    Dummy rule to automatically generate the required outputs.
    '''
    input:
        unpack(get_outputs)
    log:
        'logs/rule_all.log'
    benchmark:
        'benchmarks/rule_all.txt'

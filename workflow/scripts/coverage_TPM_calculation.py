'''
Calculate coverage and Transcripts Per Million (TPM) for each exon/gene.
'''


import logging
import os
import sys
import pandas as pd
from utils import setup_logging


if __name__ == '__main__':

    setup_logging(snakemake)

    logging.info('Getting data from snakemake')
    exon_or_gene = snakemake.wildcards.exon_or_gene
    read_quantif = snakemake.input.read_quantif
    read_length_summary = snakemake.input.read_length_summary
    output_sorted = snakemake.output.read_quantif_sorted
    cov_tsv = snakemake.output.cov_tsv
    tpm_tsv = snakemake.output.tpm_tsv

    output_dir = os.path.dirname(output_sorted)
    os.makedirs(output_dir, exist_ok=True)

    logging.info(f'Importing <{exon_or_gene}> quantification data from <{read_quantif}>')
    reads = pd.read_csv(read_quantif, sep='\t', comment='#')
    reads.rename(columns={reads.columns[-1]: 'Reads_quant'}, inplace=True)

    # Exons or genes tables have to be processed differently
    if exon_or_gene not in ['exons', 'genes']:
        logging.error(f'<exon_or_gene> wildcard should be <exons> or <genes>.')
        logging.error(f'\tCurrent value: {exon_or_gene}')
        sys.exit(1)
    if exon_or_gene == 'exons':
        logging.info('Sorting <exon> table by Chromosome then Start position')
        reads.sort_values(['Chr', 'Start'], ascending=[True, True], inplace=True)
    else:
        logging.info('Sorting <gene> table by Chromosome then Start position')
        # New columns are simpler and will be used to properly reorder the table
        logging.info('\tCreating temporary columns')
        # We get the unique Chr ID with set
        reads['Chr_new'] = reads['Chr'].apply(lambda x: ''.join(set(x.split(';'))))
        # We select the start of the first exon
        reads['Start_new'] = reads['Start'].apply(lambda x: int(x.split(';')[0]))
        logging.info('\tSorting table')
        reads.sort_values(['Chr_new', 'Start_new'], ascending=[True, True],
                          inplace=True)
        logging.info('\tRemoving temporary columns')
        reads.drop(['Chr_new', 'Start_new'], axis='columns', inplace=True)

    logging.info(f'Saving sorted table in <{output_sorted}>')
    reads.to_csv(output_sorted, sep='\t', header=True, index=False)

    logging.info(f'Importing read lengths summary from <{read_length_summary}>')
    RL_table = pd.read_csv(read_length_summary, sep='\t', header=None,
                           names=['Read_length', 'Read_number'])

    logging.info('Estimating average read length')
    # Calculates number of bases for each read length
    RL_table['Number_of_base'] = RL_table['Read_length'] * RL_table['Read_number']
    # Calculates average read_length
    read_length = RL_table['Number_of_base'].sum() / RL_table['Read_number'].sum()

    logging.info(f'Calculating <{exon_or_gene}> coverage')
    reads['Coverage'] = reads['Reads_quant'] * read_length / reads['Length']

    logging.info(f'Saving coverage table in <{cov_tsv}>')
    reads.to_csv(cov_tsv, sep='\t', header=True, index=False)

    logging.info(f'Calculating <{exon_or_gene}> TPM')
    reads['TPM'] = reads['Coverage'] * 10**6 / reads['Coverage'].sum()

    logging.info('Deleting coverage column')
    reads.drop('Coverage', axis='columns', inplace=True)

    logging.info(f'Saving TPM table in <{tpm_tsv}>')
    reads.to_csv(tpm_tsv, sep='\t', header=True, index=False)
    logging.info('Done')

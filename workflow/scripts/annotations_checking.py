'''
This script checks if an annotation file is in gtf format. If so, it checks the
file for empty gene_id fields and fixes them using transcript_ids. It also
creates pseudogenic transcripts for pseudogenes that are missing children
features. If not, it just renames the file to keep the workflow going.
'''


import logging
import mmap
import os
import re
import sys
from utils import setup_logging


# Functions to create a new gtf row from a pre-existing one
def create_transcript_line(line, gene_id):
    '''
    Create a pseudo-transcript line for a gtf annotation file using a pre-
    existing pseudogene line by adding and updating specific fields.
    Inputs:
        - line: string containing an entire pseudogene line
        - gene_id: value of the gene_id attribute found in the 9th column
    Return:
        - Updated string containing an entire pseudo-transcript line
    '''
    # Change feature type to transcript
    new_line = line.replace('\tgene\t', '\ttranscript\t')
    # Create transcript_id if it doesn't exist
    if 'transcript_id' not in new_line:
        new_line = re.sub(f'(gene_id "{gene_id}"; )',
                          rf'\1transcript_id "{gene_id}_pseudogenic_transcript"; ',
                          new_line)
    # Update transcript_id if it is empty
    elif 'transcript_id ""' in new_line:
        new_line = new_line.replace('transcript_id ""',
                                    f'transcript_id "{gene_id}_pseudogenic_transcript"')
    # Change gbkey
    new_line = new_line.replace('gbkey "Gene"', 'gbkey "pseudogenic_transcript"')
    # Remove gene_biotype that doesn't appear in a transcript
    new_line = new_line.replace('gene_biotype "pseudogene"; ', '')
    product_biotype = 'product "putative pseudogenic transcript"; transcript_biotype "pseudogenic_transcript"; '
    # Add product and biotype at the end
    new_line = new_line.rstrip('\n') + product_biotype + '\n'
    return new_line


def create_exon_line(line, gene_id):
    '''
    Create a pseudo-exonic line for a gtf annotation file using a pre-
    existing pseudo-transcript line by adding and updating specific fields.
    Inputs:
        - line: string containing an entire pseudo-transcript line
        - gene_id: value of the gene_id attribute found in the 9th column
    Return:
        - Updated string containing an entire pseudo-exonic line
    '''
    # Change feature type to exon
    new_line = line.replace('\ttranscript\t', '\texon\t')
    # Create ID if it doesn't exist
    if 'ID "' not in new_line:
        new_line = re.sub(f'(transcript_id "{gene_id}_pseudogenic_transcript"; )',
                          rf'\1ID "{gene_id}_pseudogenic_exon"; ', new_line)
    # Change gbkey
    new_line = new_line.replace('gbkey "pseudogenic_transcript"',
                                'exon_number "1"; gbkey "pseudogenic_exon"')
    return new_line


if __name__ == '__main__':

    setup_logging(snakemake)

    logging.info('Getting data from snakemake')
    broken_gtf = snakemake.input.broken_gtf
    noempty_gtf = snakemake.output.noempty_gtf

    output_dir = os.path.dirname(noempty_gtf)
    os.makedirs(output_dir, exist_ok=True)

    logging.info(f'Testing format of <{broken_gtf}>')
    with open(broken_gtf, 'rb', 0) as file, \
            mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ) as s:
        file_format = (s.find(b'gff') != -1 or s.find(b'ID=') != -1)
    if file_format:
        logging.info('\tThis annotation file is in gff format. It does not need')
        logging.info('\tto be processed by this script and will be further')
        logging.info('\tchecked in the next step of the workflow')
        os.rename(broken_gtf, noempty_gtf)
        logging.info(f'<{broken_gtf}> renamed to <{noempty_gtf}>')
        sys.exit(0)

    logging.info('\tThis annotation file is in gtf format. Script will continue')
    logging.info('Fixing empty gene_id and missing pseudogenic transcript in file')
    regex_pattern_txp = 'gene_id ""; transcript_id "([-a-zA-Z0-9_.]+)"'
    regex_pattern_gene = 'gene_id "([-a-zA-Z0-9_.]+)"'
    gene_count = pseudo_transcript_count = pseudo_exon_count = 0
    backup = ''
    with open(broken_gtf) as annotation_file, open(noempty_gtf, 'w') as output:
        for line_nb, line in enumerate(annotation_file, 1):
            if 'gene_id ""' in line:
                if 'transcript_id ""' in line.lower():
                    # gene_id and transcript_id are mandatory fields
                    # At least one should be filled
                    logging.error(f'Both gene_id and transcript_id are empty in line <{line_nb}>')
                    sys.exit(1)
                transcript_id = re.search(regex_pattern_txp, line).group(1)
                line = line.replace('gene_id ""',
                                    f'gene_id "gene_{transcript_id}"')
                gene_count += 1
                logging.info(f'\tLine <{line_nb}>: empty gene_id fixed using transcript_id <{transcript_id}>')
            if backup:
                output.write(backup)
                if 'gene_biotype "pseudogene"' in backup:
                    # Get gene_id from previous line
                    backup_gene_id = re.search(regex_pattern_gene, backup).group(1)
                    if ('\ttranscript\t' not in line
                            or ('\ttranscript\t' in line
                                and backup_gene_id not in line)):
                        # For everything that isn't a transcript or a transcript
                        # that is not linked to the pseudogene of interest
                        new_transcript = create_transcript_line(backup,
                                                                backup_gene_id)
                        pseudo_transcript_count += 1
                        output.write(new_transcript)
                        logging.info(f'\tAdded pseudogenic transcript for pseudogene <{backup_gene_id}>')
                        if ('\texon\t' not in line
                                or ('\texon\t' in line
                                    and backup_gene_id not in line)):
                            # For everything that isn't an exon or an exon
                            # that is not linked to the pseudogene of interest
                            new_exon = create_exon_line(new_transcript,
                                                        backup_gene_id)
                            pseudo_exon_count += 1
                            output.write(new_exon)
                            logging.info(f'\t\tAdded pseudogenic exon for pseudogene <{backup_gene_id}>')
            backup = line
    # Process last line of file
    with open(noempty_gtf, 'a') as output:
        output.write(backup)
        if 'gene_biotype "pseudogene"' in backup:
            # Get gene_id from last line
            backup_gene_id = re.search(regex_pattern_gene, backup).group(1)
            new_transcript = create_transcript_line(backup, backup_gene_id)
            pseudo_transcript_count += 1
            output.write(new_transcript)
            logging.info(f'\tAdded pseudogenic transcript for pseudogene <{backup_gene_id}>')
            new_exon = create_exon_line(backup, backup_gene_id)
            pseudo_exon_count += 1
            output.write(new_exon)
            logging.info(f'\t\tAdded pseudogenic exon for pseudogene <{backup_gene_id}>')
    logging.info(f'Total number of gene_id fixed: <{gene_count}>')
    logging.info(f'Total number of pseudogenic transcripts added: <{pseudo_transcript_count}>')
    logging.info(f'Total number of pseudogenic exons added: <{pseudo_exon_count}>')

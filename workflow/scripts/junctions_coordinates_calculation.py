'''
Calculate coordinates of exon-exon junctions.
'''


import logging
import os
import pandas as pd
from utils import setup_logging


# Constants
COL_NAMES_INPUT = ['Chromosome', 'Chrom_Start', 'Chrom_End',
                   'Junction_name', 'Supporting_reads', 'Strand',
                   'Left_block_size', 'Right_block_size']
COL_INDEX = list(range(0, 6)) + [12, 13]
COL_NAMES_OUTPUT = ['Chromosome', 'Junction_start', 'Junction_end',
                    'Junction_name', 'Supporting_reads', 'Strand']


if __name__ == '__main__':

    setup_logging(snakemake)

    logging.info('Getting data from snakemake')
    junctions_bed = snakemake.input.junctions_bed
    coordinates = snakemake.output.coordinates

    output_dir = os.path.dirname(coordinates)
    os.makedirs(output_dir, exist_ok=True)

    logging.info(f'Importing regtools output from <{junctions_bed}>')
    input_bed = pd.read_csv(junctions_bed, sep='\t|,', engine='python',
                            names=COL_NAMES_INPUT, usecols=COL_INDEX)

    logging.info('Calculating real coordinates of exon-exon junctions')
    input_bed['Junction_start'] = input_bed['Chrom_Start'] + input_bed['Left_block_size']
    input_bed['Junction_end'] = input_bed['Chrom_End'] - input_bed['Right_block_size']

    logging.info('Subsetting table to columns of interest')
    logging.info(f'Saving it to <{coordinates}>')
    input_bed[COL_NAMES_OUTPUT].to_csv(coordinates, sep='\t',
                                       header=False, index=False)
    logging.info('Done')

'''
This file contains rules used to produce a variety of files usable as tracks
in a genome browser: genome coverage, exon-exon junctions...
'''


# bigwig coverage tracks
rule get_chrom_sizes:
    '''
    This rule produces a file containing the chromosome sizes in an assembly.
    '''
    input:
        fasta = rules.fasta_DL_unpacking.output.fasta
    output:
        twobit = temp('resources/assembly_files/{assembly}_genome.2bit'),
        chrom_sizes = 'resources/assembly_files/{assembly}_genome.chrom.sizes'
    log:
        'logs/assembly_files/{assembly}_chrom_sizes.log'
    benchmark:
        'benchmarks/assembly_files/{assembly}_chrom_sizes.txt'
    conda:
        '../envs/tracks.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 1024 * attempt,
        runtime = 60
    threads: 1
    shell:
        """
        echo "Converting <{input.fasta}> into 2bit file" > {log}
        faToTwoBit {input.fasta} {output.twobit} 2>> {log}
        echo "Extracting chromosome sizes from 2bit file" >> {log}
        twoBitInfo {output.twobit} stdout | sort -k2rn > {output.chrom_sizes} 2>> {log}
        echo "Results saved in <{output.chrom_sizes}>" >> {log}
        """


rule bigwig_coverage:
    '''
    This rule produces a bigwig coverage track for a sample.
    '''
    input:
        chrom_sizes = rules.get_chrom_sizes.output.chrom_sizes,
        bam_once_sorted = rules.sam_to_bam.output.bam_once_sorted
    output:
        bedgraph = temp('results/{assembly}/{sample}/{sample}_genome_coverage.bedGraph'),
        sorted_bedgraph = temp('results/{assembly}/{sample}/{sample}_genome_coverage_sorted.bedGraph'),
        bigwig = 'results/{assembly}/{sample}/{sample}_genome_coverage.bw'
    log:
        'logs/{assembly}/{sample}/{sample}_genome_coverage_bw.log'
    benchmark:
        'benchmarks/{assembly}/{sample}/{sample}_genome_coverage_bw.txt'
    conda:
        '../envs/tracks.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 2048 * attempt,
        runtime = 60
    threads: 1
    shell:
        """
        echo "Creating bedgraph file from <{input.bam_once_sorted}>" > {log}
        bedtools genomecov -bg -ibam {input.bam_once_sorted} -split > {output.bedgraph} 2>> {log}
        echo "Sorting bedgraph file" >> {log}
        LC_COLLATE=C sort -k1,1 -k2,2n {output.bedgraph} > {output.sorted_bedgraph} 2>> {log}
        echo "Converting bedgraph file to bigwig" >> {log}
        bedGraphToBigWig {output.sorted_bedgraph} {input.chrom_sizes} {output.bigwig} 2>> {log}
        echo "Results saved in <{output.bigwig}>" >> {log}
        """


# Exon-exon junctions file
rule junctions_coordinates_detection:
    '''
    This rule analyses a bam file with regtools to detect exon-exon junctions.
    '''
    input:
        bam_once_sorted = rules.sam_to_bam.output.bam_once_sorted,
        strandedness_report = rules.strandedness_report.output.strandedness_report
    output:
        junctions_bed = 'results/{assembly}/{sample}/{sample}_junctions.bed'
    params:
        strandedness = lambda wildcards: get_strandedness(wildcards)[3]
    log:
        'logs/{assembly}/{sample}/{sample}_junctions_detection.log'
    benchmark:
        'benchmarks/{assembly}/{sample}/{sample}_junctions_detection.txt'
    conda:
        '../envs/tracks.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 2048 * attempt,
        runtime = 30
    threads: 1
    shell:
        """
        echo "Using regtools to detect exon-exon junctions in <{input.bam_once_sorted}>" > {log}
        regtools junctions extract -a 8 -m 20 -M 120000 -s {params.strandedness} -o {output.junctions_bed} {input.bam_once_sorted} 2>> {log}
        echo "Results saved in <{output.junctions_bed}>" >> {log}
        """


rule junctions_coordinates_calculation:
    '''
    This rule calculates the real start/end of junctions using the raw
    output of regtools.
    '''
    input:
        junctions_bed = rules.junctions_coordinates_detection.output.junctions_bed
    output:
        coordinates = 'results/{assembly}/{sample}/{sample}_junctions_coordinates.bed'
    log:
        'logs/{assembly}/{sample}/{sample}_junctions_coordinates.log'
    benchmark:
        'benchmarks/{assembly}/{sample}/{sample}_junctions_coordinates.txt'
    conda:
        '../envs/py.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 1024 * attempt,
        runtime = 30
    threads: 1
    script:
        '../scripts/junctions_coordinates_calculation.py'


rule junctions_files_merging:
    '''
    This rule gathers all the junctions files of an assembly into one bed file.
    '''
    input:
        get_junctions_files
    output:
        merged_bed = temp('results/{assembly}/{assembly}_junctions_files_merged.bed')
    log:
        'logs/{assembly}/{assembly}_junctions_files_merging.log'
    benchmark:
        'benchmarks/{assembly}/{assembly}_junctions_files_merging.txt'
    conda:
        '../envs/R.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 1024 * attempt,
        runtime = 30
    threads: 1
    shell:
        """
        echo "Counting junction files to merge" > {log}
        A=$(ls results/{wildcards.assembly}/*/*_junctions_coordinates.bed | wc -l) 2>> {log}
        echo "Found ${{A}} files" >> {log}
        echo "Summary of the files and their junction counts:" >> {log}
        wc -l results/{wildcards.assembly}/*/*_junctions_coordinates.bed >> {log}
        echo "Merging junction files" >> {log}
        cat {input} > {output.merged_bed} 2>> {log}
        echo "Results saved in <{output.merged_bed}>" >> {log}
        """


rule junctions_coordinates_merging:
    '''
    This rule totals the number of supporting reads for identical junctions
    in an assembly.
    '''
    input:
        merged_bed = rules.junctions_files_merging.output.merged_bed
    output:
        junctions = 'results/{assembly}/{assembly}_junctions.bed'
    log:
        'logs/{assembly}/{assembly}_junctions_coordinates_merging.log'
    benchmark:
        'benchmarks/{assembly}/{assembly}_junctions_coordinates_merging.txt'
    conda:
        '../envs/py.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 1024 * attempt,
        runtime = 30
    threads: 1
    script:
        '../scripts/junctions_coordinates_merging.py'

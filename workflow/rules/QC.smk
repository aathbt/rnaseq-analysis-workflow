'''
This file contains rules used to perform all sorts of Quality Checks (QCs)
during the execution of the workflow.
'''


# To avoid conflicts during choice of rule to execute
ruleorder: fastq_qc_PE > fastq_qc_SE

rule fastq_qc_PE:
    '''
    This rule performs a QC on paired-end fastq files before and after trimming.
    '''
    input:
        reads1 = rules.fastq_DL_PE.output.reads1,
        reads2 = rules.fastq_DL_PE.output.reads2,
        trim1 = rules.fastq_trim_PE.output.trim1,
        trim2 = rules.fastq_trim_PE.output.trim2
    output:
        # QC before trimming
        html1_before = 'results/{assembly}/{sample}/fastqc_reports/{sample}_fastqc_pretrim_1.html',
        zipfile1_before = 'results/{assembly}/{sample}/fastqc_reports/{sample}_fastqc_pretrim_1.zip',
        html2_before = 'results/{assembly}/{sample}/fastqc_reports/{sample}_fastqc_pretrim_2.html',
        zipfile2_before = 'results/{assembly}/{sample}/fastqc_reports/{sample}_fastqc_pretrim_2.zip',
        # QC after trimming
        html1_after = 'results/{assembly}/{sample}/fastqc_reports/{sample}_fastqc_aftertrim_1.html',
        zipfile1_after = 'results/{assembly}/{sample}/fastqc_reports/{sample}_fastqc_aftertrim_1.zip',
        html2_after = 'results/{assembly}/{sample}/fastqc_reports/{sample}_fastqc_aftertrim_2.html',
        zipfile2_after = 'results/{assembly}/{sample}/fastqc_reports/{sample}_fastqc_aftertrim_2.zip'
    params:
        wd = lambda wildcards, output: os.path.dirname(output.html1_before),
        # QC before trimming
        html1_before = 'results/{assembly}/{sample}/fastqc_reports/{sample}_1_fastqc.html',
        zipfile1_before = 'results/{assembly}/{sample}/fastqc_reports/{sample}_1_fastqc.zip',
        html2_before = 'results/{assembly}/{sample}/fastqc_reports/{sample}_2_fastqc.html',
        zipfile2_before = 'results/{assembly}/{sample}/fastqc_reports/{sample}_2_fastqc.zip',
        # QC after trimming
        html1_after = 'results/{assembly}/{sample}/fastqc_reports/{sample}_atropos_trimmed_1_fastqc.html',
        zipfile1_after = 'results/{assembly}/{sample}/fastqc_reports/{sample}_atropos_trimmed_1_fastqc.zip',
        html2_after = 'results/{assembly}/{sample}/fastqc_reports/{sample}_atropos_trimmed_2_fastqc.html',
        zipfile2_after = 'results/{assembly}/{sample}/fastqc_reports/{sample}_atropos_trimmed_2_fastqc.zip'
    log:
        'logs/{assembly}/{sample}/{sample}_fastqc.log'
    benchmark:
        'benchmarks/{assembly}/{sample}/{sample}_atropos_fastqc.txt'
    conda:
        '../envs/QC.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 2048 * attempt,
        runtime = 30
    threads: 2
    shell:
        """
        echo "Creating output directory <{params.wd}>" > {log}
        mkdir -p {params.wd}
        echo "Performing QC of reads before trimming in <{input.reads1}> and <{input.reads2}>" >> {log}
        fastqc --outdir {params.wd} --format fastq --threads {threads} --dir {params.wd} {input.reads1} {input.reads2} &>> {log}
        echo "Renaming files" >> {log}  # Renames files because we can't choose fastqc output
        mv {params.html1_before} {output.html1_before} 2>> {log}
        mv {params.zipfile1_before} {output.zipfile1_before} 2>> {log}
        mv {params.html2_before} {output.html2_before} 2>> {log}
        mv {params.zipfile2_before} {output.zipfile2_before} 2>> {log}
        echo "Performing QC of reads after trimming in <{input.trim1}> and <{input.trim2}>" >> {log}
        fastqc --outdir {params.wd} --format fastq --threads {threads} --dir {params.wd} {input.trim1} {input.trim2} &>> {log}
        echo "Renaming files" >> {log}  # Renames files because we can't choose fastqc output
        mv {params.html1_after} {output.html1_after} 2>> {log}
        mv {params.zipfile1_after} {output.zipfile1_after} 2>> {log}
        mv {params.html2_after} {output.html2_after} 2>> {log}
        mv {params.zipfile2_after} {output.zipfile2_after} 2>> {log}
        echo "Results saved in <results/{wildcards.assembly}/{wildcards.sample}/fastqc_reports/>" >> {log}
        """


rule fastq_qc_SE:
    '''
    This rule performs a QC on a single-end fastq file before and after trimming.
    '''
    input:
        reads = rules.fastq_DL_SE.output.reads,
        trim = rules.fastq_trim_SE.output.trim
    output:
        # QC before trimming
        html_before = 'results/{assembly}/{sample}/fastqc_reports/{sample}_fastqc_pretrim.html',
        zipfile_before = 'results/{assembly}/{sample}/fastqc_reports/{sample}_fastqc_pretrim.zip',
        # QC after trimming
        html_after = 'results/{assembly}/{sample}/fastqc_reports/{sample}_fastqc_aftertrim.html',
        zipfile_after = 'results/{assembly}/{sample}/fastqc_reports/{sample}_fastqc_aftertrim.zip'
    params:
        wd = lambda wildcards, output: os.path.dirname(output.html_before),
        # QC before trimming
        html_before = 'results/{assembly}/{sample}/fastqc_reports/{sample}_fastqc.html',
        zipfile_before = 'results/{assembly}/{sample}/fastqc_reports/{sample}_fastqc.zip',
        # QC after trimming
        html_after = 'results/{assembly}/{sample}/fastqc_reports/{sample}_atropos_trimmed_fastqc.html',
        zipfile_after = 'results/{assembly}/{sample}/fastqc_reports/{sample}_atropos_trimmed_fastqc.zip'
    log:
        'logs/{assembly}/{sample}/{sample}_fastqc.log'
    benchmark:
        'benchmarks/{assembly}/{sample}/{sample}_fastqc.txt'
    conda:
        '../envs/QC.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 2048 * attempt,
        runtime = 30
    threads: 1
    shell:
        """
        echo "Creating output directory <{params.wd}>" > {log}
        mkdir -p {params.wd}
        echo "Performing QC of reads before trimming in <{input.reads}>" >> {log}
        fastqc --outdir {params.wd} --format fastq --threads {threads} --dir {params.wd} {input.reads} &>> {log}
        echo "Renaming files" >> {log}  # Renames files because we can't choose fastqc output
        mv {params.html_before} {output.html_before} 2>> {log}
        mv {params.zipfile_before} {output.zipfile_before} 2>> {log}
        echo "Performing QC of reads after trimming in <{input.trim}>" >> {log}
        fastqc --outdir {params.wd} --format fastq --threads {threads} --dir {params.wd} {input.trim} &>> {log}
        echo "Renaming files" >> {log}  # Renames files because we can't choose fastqc output
        mv {params.html_after} {output.html_after} 2>> {log}
        mv {params.zipfile_after} {output.zipfile_after} 2>> {log}
        echo "Results saved in <results/{wildcards.assembly}/{wildcards.sample}/fastqc_reports/>" >> {log}
        """


rule mapping_stats_samtools:
    '''
    This rule produces mapping statistics using samtools.
    '''
    input:
        bam_once_sorted = rules.sam_to_bam.output.bam_once_sorted
    output:
        samtools_stats = 'results/{assembly}/{sample}/mapping_stats_samtools/{sample}_mapping_stats_samtools.txt',
        read_length = temp('results/{assembly}/{sample}/mapping_stats_samtools/{sample}_mapping_stats_samtools_RL.txt')
    params:
        wd_samtools = 'results/{assembly}/{sample}/mapping_stats_samtools/'
    log:
        'logs/{assembly}/{sample}/{sample}_mapping_stats_samtools.log'
    benchmark:
        'benchmarks/{assembly}/{sample}/{sample}_mapping_stats_samtools.txt'
    conda:
        '../envs/QC.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 1024 * attempt,
        runtime = 30
    threads: 2
    shell:
        """
        echo "Using samtools to produce mapping statistics of <{input.bam_once_sorted}>" > {log}
        samtools stats --threads {threads} {input.bam_once_sorted} > {output.samtools_stats} 2>> {log}
        echo "Extracting read lengths from statistics output" >> {log}
        grep ^RL {output.samtools_stats} | cut -f 2- > {output.read_length} 2>> {log}
        echo "Plotting results" >> {log}
        plot-bamstats -p {params.wd_samtools}{wildcards.sample}_mapping_stats_samtools {output.samtools_stats} 2>> {log}
        echo "Results saved in <results/{wildcards.assembly}/{wildcards.sample}/mapping_stats_samtools/>" >> {log}
        """


rule mapping_stats_qualimap_bamqc:
    '''
    This rule produces mapping statistics using Qualimap.
    '''
    input:
        bam_once_sorted = rules.sam_to_bam.output.bam_once_sorted,
        strandedness_report = rules.strandedness_report.output.strandedness_report
    output:
        bamqc_pdf = 'results/{assembly}/{sample}/mapping_stats_qualimap_bamqc/{sample}_mapping_stats_qualimap_bamqc.pdf',
        bamqc_html = 'results/{assembly}/{sample}/mapping_stats_qualimap_bamqc/{sample}_mapping_stats_qualimap_bamqc.html'
    params:
        wd_bamqc = 'results/{assembly}/{sample}/mapping_stats_qualimap_bamqc/',
        strandedness_bamqc = lambda wildcards: get_strandedness(wildcards)[1],
    log:
        'logs/{assembly}/{sample}/{sample}_mapping_stats_qualimap_bamqc.log'
    benchmark:
        'benchmarks/{assembly}/{sample}/{sample}_mapping_stats_qualimap_bamqc.txt'
    conda:
        '../envs/QC.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 12288 * attempt,
        runtime = 120
    threads: 4
    shell:
        """
        echo "Using Qualimap to produce bamqc mapping statistics of <{input.bam_once_sorted}>" > {log}
        qualimap bamqc -bam {input.bam_once_sorted} -hm 3 --collect-overlap-pairs -nr 1000 -nw 500 --sequencing-protocol {params.strandedness_bamqc} \
        -outformat PDF:HTML -outdir {params.wd_bamqc} -nt {threads} --java-mem-size={resources.mem_mb}M &>> {log}
        echo "Renaming files" >> {log}  # Renames files because we can't choose qualimap output
        mv {params.wd_bamqc}report.pdf {output.bamqc_pdf} 2>> {log}
        mv {params.wd_bamqc}qualimapReport.html {output.bamqc_html} 2>> {log}
        echo "Results saved in <{params.wd_bamqc}>" >> {log}
        """


rule mapping_stats_qualimap_rnaseq:
    '''
    This rule produces rnaseq statistics using Qualimap.
    '''
    input:
        bam_once_sorted = rules.sam_to_bam.output.bam_once_sorted,
        annotation = lambda wildcards: 'resources/annotations/{assembly}_all_annotations_fixed.gtf',
        strandedness_report = rules.strandedness_report.output.strandedness_report
    output:
        rnaseq_pdf = 'results/{assembly}/{sample}/mapping_stats_qualimap_rnaseq/{sample}_mapping_stats_qualimap_rnaseq.pdf',
        rnaseq_html = 'results/{assembly}/{sample}/mapping_stats_qualimap_rnaseq/{sample}_mapping_stats_qualimap_rnaseq.html'
    params:
        wd_rnaseq = 'results/{assembly}/{sample}/mapping_stats_qualimap_rnaseq/',
        strandedness_rnaseq = lambda wildcards: get_strandedness(wildcards)[2],
        pe = (
            lambda wildcards, input: ["-pe"]
            if samples_dict[wildcards.sample]['Sequencing_type'] == "PE"
            else [""]
        )
    log:
        'logs/{assembly}/{sample}/{sample}_mapping_stats_qualimap_rnaseq.log'
    benchmark:
        'benchmarks/{assembly}/{sample}/{sample}_mapping_stats_qualimap_rnaseq.txt'
    conda:
        '../envs/QC.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 12288 * attempt,
        runtime = 120
    threads: 4
    shell:
        """
        echo "Using Qualimap to produce rnaseq mapping statistics of <{input.bam_once_sorted}>" > {log}
        qualimap rnaseq {params.pe} -bam {input.bam_once_sorted} -gtf {input.annotation} -a uniquely-mapped-reads -npb 100 -ntb 1000 --sequencing-protocol {params.strandedness_rnaseq} \
        -outformat PDF:HTML -outdir {params.wd_rnaseq} --java-mem-size={resources.mem_mb}M &>> {log}
        echo "Renaming files" >> {log}  # Renames files because we can't choose qualimap output
        mv {params.wd_rnaseq}report.pdf {output.rnaseq_pdf} 2>> {log}
        mv {params.wd_rnaseq}qualimapReport.html {output.rnaseq_html} 2>> {log}
        echo "Results saved in <{params.wd_rnaseq}>" >> {log}
        """


rule multi_qc:
    '''
    This rule gathers all QC reports produced during the workflow execution
    in a MultiQC report in html format.
    '''
    input:
        # These inputs won't actually be used but it makes sure multiqc
        # doesn't miss anything
        fastqc = get_fastqc,
        hisat2 = rules.read_mapping.output.report,
        samtools = rules.mapping_stats_samtools.output.samtools_stats,
        qualimap_bamqc = rules.mapping_stats_qualimap_bamqc.output.bamqc_pdf,
        qualimap_rnaseq = rules.mapping_stats_qualimap_rnaseq.output.rnaseq_pdf,
        feature_counts_genes = rules.reads_quantification_genes.output.gene_summary,
        feature_counts_exons = rules.reads_quantification_exons.output.exon_summary
    output:
        report = 'results/{assembly}/{sample}/multiqc_report/{sample}_multiqc_report.html'
    params:
        config_multiqc = config['multiqc'],
        wd = lambda wildcards, output: os.path.dirname(os.path.dirname(output.report))  # To avoid adding pathlib in QC environment
    log:
        'logs/{assembly}/{sample}/{sample}_multiQC.log'
    benchmark:
        'benchmarks/{assembly}/{sample}/{sample}_multiQC.txt'
    conda:
        '../envs/QC.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 1024 * attempt,
        runtime = 15
    threads: 1
    shell:
        """
        echo "Gathering all QC reports for {wildcards.sample}" > {log}
        multiqc --title "MultiQC report of {wildcards.sample}" --filename {output.report} -s -f \
        --config {params.config_multiqc} --no-megaqc-upload --profile-runtime --zip-data-dir {params.wd} &>> {log}
        echo "Results saved in <{output.report}>" >> {log}
        """


rule coverage_TPM_calculation:
    '''
    This rule calculates exons/genes coverage/TPM in a sample.
    '''
    input:
        read_quantif = 'results/{assembly}/{sample}/{sample}_{exon_or_gene}_read_quantification.tsv',
        read_length_summary = rules.mapping_stats_samtools.output.read_length
    output:
        read_quantif_sorted = 'results/{assembly}/{sample}/{sample}_{exon_or_gene}_read_quantification_sorted.tsv',
        cov_tsv = 'results/{assembly}/{sample}/{sample}_{exon_or_gene}_coverage.tsv',
        tpm_tsv = 'results/{assembly}/{sample}/{sample}_{exon_or_gene}_TPM.tsv'
    log:
        'logs/{assembly}/{sample}/{sample}_{exon_or_gene}_coverage_calculation.log'
    benchmark:
        'benchmarks/{assembly}/{sample}/{sample}_{exon_or_gene}_coverage_calculation.txt'
    conda:
        '../envs/py.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 1024 * attempt,
        runtime = 30
    threads: 1
    script:
        '../scripts/coverage_TPM_calculation.py'


rule coverage_TPM_plotting:
    '''
    This rule plots exons/genes coverage/TPM in a sample.
    '''
    input:
        cov_tsv = rules.coverage_TPM_calculation.output.cov_tsv,
        tpm_tsv = rules.coverage_TPM_calculation.output.tpm_tsv
    output:
        cov_tpm_pdf = 'results/{assembly}/{sample}/{sample}_{exon_or_gene}_coverage_TPM.pdf'
    log:
        'logs/{assembly}/{sample}/{sample}_{exon_or_gene}_coverage_plotting.log'
    benchmark:
        'benchmarks/{assembly}/{sample}/{sample}_{exon_or_gene}_coverage_plotting.txt'
    conda:
        '../envs/R.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 1024 * attempt,
        runtime = 30
    threads: 2
    script:
        '../scripts/coverage_TPM_plotting.R'


rule genome_coverage_calculation:
    '''
    This rule produces genome coverage data in a sample.
    '''
    input:
        bam_once_sorted = rules.sam_to_bam.output.bam_once_sorted
    output:
        all_cov_bed = temp('results/{assembly}/{sample}/{sample}_coverage_all.bed'),
        gnm_cov_bed = 'results/{assembly}/{sample}/{sample}_genome_coverage.bed'
    log:
        'logs/{assembly}/{sample}/{sample}_genome_coverage_calculation.log'
    benchmark:
        'benchmarks/{assembly}/{sample}/{sample}_genome_coverage_calculation.txt'
    conda:
        '../envs/tracks.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 1024 * attempt,
        runtime = 30
    threads: 1
    shell:
        """
        echo "Calculating chromosomes and whole genome coverage values in <{input.bam_once_sorted}>" > {log}
        bedtools genomecov -ibam {input.bam_once_sorted} -split > {output.all_cov_bed} 2> {log}
        echo "Extracting genome coverage values" >> {log}
        grep -e "^genome" {output.all_cov_bed} > {output.gnm_cov_bed}
        echo "Results saved in <{output.gnm_cov_bed}>" >> {log}
        """


rule genome_coverage_plotting:
    '''
    This rule calculates and plots genome coverage data in a sample.
    '''
    input:
        gnm_cov_bed = rules.genome_coverage_calculation.output.gnm_cov_bed
    output:
        gnm_cov_pdf = 'results/{assembly}/{sample}/{sample}_genome_coverage.pdf',
    log:
        'logs/{assembly}/{sample}/{sample}_genome_coverage_plotting.log'
    benchmark:
        'benchmarks/{assembly}/{sample}/{sample}_genome_coverage_plotting.txt'
    conda:
        '../envs/R.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 1024 * attempt,
        runtime = 30
    threads: 2
    script:
        '../scripts/genome_coverage_plotting.R'

'''
This file contains rules used to pre-process reads in fastq files: calculation
of sequencing error-rate, detection of adapters and trimming of low-quality
reads. These steps are performed with Atropos (https://doi.org/10.7717/peerj.3720).
'''


# To avoid conflicts during choice of rule to execute
ruleorder: fastq_error_rate_PE > fastq_error_rate_SE

rule fastq_error_rate_PE:
    '''
    This rule determines the sequencing error-rate in a paired-end reads fastq file.
    It will be used during trimming of said fastq.
    '''
    input:
        reads1 = rules.fastq_DL_PE.output.reads1,
        reads2 = rules.fastq_DL_PE.output.reads2
    output:
        error = 'results/{assembly}/{sample}/{sample}_atropos_error_PE.txt',
        error_rate = 'results/{assembly}/{sample}/{sample}_atropos_error_rate_PE.txt'
    log:
        'logs/{assembly}/{sample}/{sample}_atropos_error_rate.log'
    benchmark:
        'benchmarks/{assembly}/{sample}/{sample}_atropos_error_rate.txt'
    conda:
        '../envs/read_preprocessing.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 1024 * attempt,
        runtime = 30
    threads: 1
    shell:
        """
        echo "Estimating error-rate in <{input.reads1}> and <{input.reads2}>" > {log}
        atropos error -pe1 {input.reads1} -pe2 {input.reads2} -a quality -o {output.error} &>> {log}
        echo "Parsing results" >> {log}
        tail -n 1 {output.error} | awk '{{print substr($0,13,4);}}' | awk '{{printf $0/10}}' > {output.error_rate} 2>> {log}
        echo "Results saved in <{output.error_rate}>" >> {log}
        """

rule fastq_error_rate_SE:
    '''
    This rule determines the sequencing error-rate in a single-end reads fastq file.
    It will be used during trimming of said fastq.
    '''
    input:
        reads = rules.fastq_DL_SE.output.reads
    output:
        error = 'results/{assembly}/{sample}/{sample}_atropos_error_SE.txt',
        error_rate = 'results/{assembly}/{sample}/{sample}_atropos_error_rate_SE.txt'
    log:
        'logs/{assembly}/{sample}/{sample}_atropos_error_rate.log'
    benchmark:
        'benchmarks/{assembly}/{sample}/{sample}_atropos_error_rate.txt'
    conda:
        '../envs/read_preprocessing.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 1024 * attempt,
        runtime = 30
    threads: 1
    shell:
        """
        echo "Estimating error-rate in <{input.reads}>" > {log}
        atropos error -se {input.reads} -a quality -o {output.error} &>> {log}
        echo "Parsing results" >> {log}
        tail -n 1 {output.error} | awk '{{print substr($0,13,4);}}' | awk '{{printf $0/10}}' > {output.error_rate} 2>> {log}
        echo "Results saved in <{output.error_rate}>" >> {log}
        """


# To avoid conflicts during choice of rule to execute
ruleorder: fastq_detect_PE > fastq_detect_SE

rule fastq_detect_PE:
    '''
    This rule detects the adapters to trim in a paired-end reads fastq file.
    They will be used during trimming of said fastq.
    '''
    input:
        reads1 = rules.fastq_DL_PE.output.reads1,
        reads2 = rules.fastq_DL_PE.output.reads2
    output:
        adapters1 = 'results/{assembly}/{sample}/{sample}_atropos_adapters_1.fasta',
        adapters2 = 'results/{assembly}/{sample}/{sample}_atropos_adapters_2.fasta'
    params:
        tmp = 'results/{assembly}/{sample}/{sample}_adapters_tmp'
    log:
        'logs/{assembly}/{sample}/{sample}_atropos_adapters.log'
    benchmark:
        'benchmarks/{assembly}/{sample}/{sample}_atropos_adapters.txt'
    conda:
        '../envs/read_preprocessing.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 1024 * attempt,
        runtime = 30
    threads: 1
    shell:
        """
        echo "Detecting adapters in <{input.reads1}> and <{input.reads2}>" > {log}
        atropos detect -pe1 {input.reads1} -pe2 {input.reads2} --no-cache-contaminants -O fasta -o {params.tmp} &>> {log}
        echo "Renaming files" >> {log}
        mv {params.tmp}.0.fasta {output.adapters1} 2>> {log}
        mv {params.tmp}.1.fasta {output.adapters2} 2>> {log}
        echo "Results saved in <{output.adapters1}> and <{output.adapters2}>" >> {log}
        """

rule fastq_detect_SE:
    '''
    This rule detects the adapters to trim in a single-end reads fastq file.
    They will be used during trimming of said fastq.
    '''
    input:
        reads = rules.fastq_DL_SE.output.reads
    output:
        adapter = 'results/{assembly}/{sample}/{sample}_atropos_adapters.fasta'
    params:
        tmp = 'results/{assembly}/{sample}/{sample}_adapters_tmp'
    log:
        'logs/{assembly}/{sample}/{sample}_atropos_adapters.log'
    benchmark:
        'benchmarks/{assembly}/{sample}/{sample}_atropos_adapters.txt'
    conda:
        '../envs/read_preprocessing.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 1024 * attempt,
        runtime = 30
    threads: 1
    shell:
        """
        echo "Detecting adapters in <{input.reads}>" > {log}
        atropos detect -se {input.reads} --no-cache-contaminants -O fasta -o {params.tmp} &>> {log}
        echo "Renaming file" >> {log}
        mv {params.tmp}.0.fasta {output.adapter} 2>> {log}
        echo "Results saved in <{output.adapter}>" >> {log}
        """


# To avoid conflicts during choice of rule to execute
ruleorder: fastq_trim_PE > fastq_trim_SE

rule fastq_trim_PE:
    '''
    This rule trims paired-end reads to improve their quality. Specifically, it removes:
    - Adapters
    - Low quality bases
    - A stretches longer than 20 bases
    - N stretches
    Note: this rule caps the sequencing error-rate parameter (-e) to 0.1
    when it's too high to avoid crashes during trimming
    '''
    input:
        reads1 = rules.fastq_DL_PE.output.reads1,
        reads2 = rules.fastq_DL_PE.output.reads2,
        adapters1 = rules.fastq_detect_PE.output.adapters1,
        adapters2 = rules.fastq_detect_PE.output.adapters2,
        error_rate = rules.fastq_error_rate_PE.output.error_rate
    output:
        trim1 = temp('results/{assembly}/{sample}/{sample}_atropos_trimmed_1.fastq'),
        trim2 = temp('results/{assembly}/{sample}/{sample}_atropos_trimmed_2.fastq'),
        report = 'results/{assembly}/{sample}/{sample}_atropos_trimming_report.txt'
    log:
        'logs/{assembly}/{sample}/{sample}_atropos_trimming.log'
    benchmark:
        'benchmarks/{assembly}/{sample}/{sample}_atropos_trimming.txt'
    conda:
        '../envs/read_preprocessing.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 16384 * attempt,
        runtime = 300
    threads: 8
    shell:
        """
        echo "Trimming reads in <{input.reads1}> and <{input.reads2}>" > {log}
        if (( $(echo $(< {input.error_rate})'>0.1' | bc -l) ))
        then
            atropos trim -a "A{{20}}" -A "A{{20}}" -a file:{input.adapters1} -A file:{input.adapters2} \
            -n 2 -pe1 {input.reads1} -pe2 {input.reads2} -o {output.trim1} -p {output.trim2} \
            -q 20,20 --minimum-length 25 --trim-n --preserve-order --max-n 10 --threads {threads} \
            --stats post -e 0.1 --no-cache-adapters --report-file {output.report} &>> {log}
        else
            atropos trim -a "A{{20}}" -A "A{{20}}" -a file:{input.adapters1} -A file:{input.adapters2} \
            -n 2 -pe1 {input.reads1} -pe2 {input.reads2} -o {output.trim1} -p {output.trim2} \
            -q 20,20 --minimum-length 25 --trim-n --preserve-order --max-n 10 --threads {threads} \
            --stats post -e $(< {input.error_rate}) --no-cache-adapters --report-file {output.report} &>> {log}
        fi
        echo "Results saved in <{output.trim1}> and <{output.trim2}>" >> {log}
        """

rule fastq_trim_SE:
    '''
    This rule trims single-end reads to improve their quality. Specifically, it removes:
    - Adapters
    - Low quality bases
    - A stretches longer than 20 bases
    - N stretches
    Note: this rule caps the sequencing error-rate parameter (-e) to 0.1
    when it's too high to avoid crashes during trimming
    '''
    input:
        reads = rules.fastq_DL_SE.output.reads,
        adapter = rules.fastq_detect_SE.output.adapter,
        error_rate = rules.fastq_error_rate_SE.output.error_rate,
    output:
        trim = temp('results/{assembly}/{sample}/{sample}_atropos_trimmed.fastq'),
        report = 'results/{assembly}/{sample}/{sample}_atropos_trimming_report.txt'
    log:
        'logs/{assembly}/{sample}/{sample}_atropos_trimming.log'
    benchmark:
        'benchmarks/{assembly}/{sample}/{sample}_atropos_trimming.txt'
    conda:
        '../envs/read_preprocessing.yaml'
    resources:
        mem_mb = lambda wildcards, attempt: 16384 * attempt,
        runtime = 240
    threads: 8
    shell:
        """
        echo "Trimming reads in <{input.reads}>" > {log}
        if (( $(echo $(< {input.error_rate})'>0.1' | bc -l) ))
        then
            atropos trim -a "A{{20}}" -a file:{input.adapter} -n 2 -se {input.reads} \
            -o {output.trim} -q 20,20 --minimum-length 25 --trim-n --preserve-order \
            --max-n 10 --threads {threads} --stats post -e 0.1 --no-cache-adapters \
            --report-file {output.report} &>> {log}
        else
            atropos trim -a "A{{20}}" -a file:{input.adapter} -n 2 -se {input.reads} \
            -o {output.trim} -q 20,20 --minimum-length 25 --trim-n --preserve-order \
            --max-n 10 --threads {threads} --stats post -e $(< {input.error_rate}) \
            --no-cache-adapters --report-file {output.report} &>> {log}
        fi
        echo "Results saved in <{output.trim}>" >> {log}
        """
